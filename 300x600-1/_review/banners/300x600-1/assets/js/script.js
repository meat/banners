/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
	if (typeof window.CustomEvent === 'function') {
		return false;
	}

	function CustomEvent(event, params) {
		params = params || { bubbles: false, cancelable: false, detail: undefined };
		var evt = document.createEvent('CustomEvent');
		evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
		return evt;
	}

	CustomEvent.prototype = window.Event.prototype;
	window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {
	var tl;
	var win = window;

	function doClickTag() {
		window.open(window.clickTag);
	}

	function initTimeline() {
		document.querySelector('#ad .banner').style.display = 'block';
		document.getElementById('ad').addEventListener('click', doClickTag);
		createTimeline();
	}

	function createTimeline() {
		tl = new TimelineMax({
			delay: 0.25,
			onStart: updateStart,
			onComplete: updateComplete,
			repeat: -1,
			onUpdate: updateStats
		});
		// ---------------------------------------------------------------------------

		tl
			.add('frame1')
			.to('.js-type1', 1.5, { x: '120%', ease: Power3.easeInOut }, 'frame1')
			.from('.js-type2', 1.5, { x: '-120%', ease: Power3.easeInOut }, 'frame1');

		// ---------------------------------------------------------------------------

		tl
			.add('frame2')
			.to('.js-type2', 1.5, { x: '100%', ease: Power3.easeInOut }, 'frame2')
			.from('.js-type3', 1.5, { x: '-120%', ease: Power3.easeInOut }, 'frame2');

		// ---------------------------------------------------------------------------

		tl
			.add('frame3')
			.to('.js-type3', 1.5, { x: '120%', ease: Power3.easeInOut }, 'frame3+=2')
			.from('.js-type4', 1.5, { x: '-120%', ease: Power3.easeInOut }, 'frame3+=2');
	}

	function updateStart() {
		var start = new CustomEvent('start', {
			detail: { hasStarted: true }
		});
		win.dispatchEvent(start);
	}

	function updateComplete() {
		var complete = new CustomEvent('complete', {
			detail: { hasStopped: true }
		});
		win.dispatchEvent(complete);
	}

	function updateStats() {
		var statistics = new CustomEvent('stats', {
			detail: {
				totalTime: tl.totalTime(),
				totalProgress: tl.totalProgress(),
				totalDuration: tl.totalDuration()
			}
		});
		win.dispatchEvent(statistics);
	}

	function getTimeline() {
		return tl;
	}

	return {
		init: initTimeline,
		get: getTimeline
	};
})();

// Banner Init
// ====================================================================================================
timeline.init();
